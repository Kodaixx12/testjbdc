package jdbcsample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {

        try (
                Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", // "jdbc:postgresql://<場所>:<ポート>/<データベース名>"
                        "nakatanikoudai",
                        "inzaghi");
                Statement statement =    connection.createStatement();
                ResultSet resultSet = statement.executeQuery(

                        "select * from test3"
                        );
                ){

            List<String> columns = new ArrayList<String>();

            ResultSetMetaData rsmd = resultSet.getMetaData();
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                columns.add(rsmd.getColumnName(i));
            }

            System.out.println(resultSet);

            while (resultSet.next()) {
                System.out.println("\ncount:" + resultSet.getRow());

                columns.stream().forEach((i)->{try {
                    System.out.println(i + ":" + resultSet.getString(i));
                } catch (SQLException e) {
                    e.printStackTrace();
                }});
            }


        }
    }

}
